# 3D Project Structure Template

TLDR: Check the end of this document to get a preview of the directory tree.

## English

Since I know a lot of people struggle to organize their 3D projects, I decided to make this template for everyone to have some foundation to start on. It does not need to be a Blender project, it can be applied with any software. Highly influenced by Andy Goralczyk's talk ["The Production Pipeline of Spring"](https://www.youtube.com/watch?v=aR3yNNGK_sc). Feel free to use it and share it however you want! No need to credit me :) 

### First step: Rename the top-level folder with the name of the project.

-Then:

--audio: all the audio goes: dialogues, foley, soundtrack ("ost"), and sound effects ("sfx");

--docs: here are all the documents to organize (spreadsheets and breakdowns into "management") and the script ("script"), programs, addons/plugins and scripts ("software"), etc .;

--image: all images, such as brush alphas ("alphas"), concept art ("concept"), colorscript ("colorscript"), HDRIs ("HDRI"), IES lights ("IES") , mattes for the backgrounds ("mattes"), reference images ("refs"), storyboard ("storyboard"), non-final textures and the like ("textures");

--lib: all 3d models and rigs go here, along with everything related;

---char: here are the characters. Geometries/meshes are names with a "mdl" suffix, rigs with a "rig" suffix, and models with materials with a "shd" suffix, while the final version doesn't use any suffix, and old versions go to "old";

---envs: here are the elements that make up the scenarios (trees, mountains, furniture, signage, buildings, etc.);

---props: here are the assets/props (only things that characters will interact with);

---set: here are the sets (files with envs, props, and chars already linked/referenced);

--scenes: here are the different stages of the film ("anim", "lit", "render"). All the scenes of the project are numbered chronologically in their own folder of the form "01-scene-name" (number that indicates the order and name that describes the scene), and are repeated in each stage. In the case of an episodic project, create folders corresponding to each episode and within them the scenes corresponding to the episode;

---01-scene_name: here are all the shots of each scene in particular, numbered chronologically in their own folder of the form "0010" (number of the shot -from 10 to 10, in case you need to add shots in between-) ;

----0010: 

-----anim: all versions of the animation-only shot, named in the form "01_0010_ANIM_A" (scene number, shot number -10 to 10, in case you need to add shots in between-, name of the stage, and version of the shot -using a letter-);

-----lit: all versions of the already animated shot, with its appropriate lighting, named in the form "01_0010_LIT_A";

-----render: all rendered frames (go in "frames", 01_0010_RENDER_0000.png), and the plates, if applicable (go in "plates", 01_0010_RENDER_plate_A.png);

--video: here are all the videos;

---animatic: the animatic.blend of the animatic (animatic_A.blend, etc.), and the folder to render the animatic ("render");

---compo: the compo.blend that combines everything (compo_A.blend, etc.);

---layout: the layout.blend of the layout (layout_A.blend, etc.), and the folder to render the layout ("render");

---master: final video render that comes out of the compo.blend;

---refs: reference videos of all kinds;


PS: Feel free to modify it as it suits you in your project. It's, more than anything, just a reference to keep in mind. If you have any suggestions or if you discovered a mistake, feel free to tell me in an Issue here or on social networks (@andycuccaro).

---

## Español

Como sé que muchas personas luchan por organizar sus proyectos 3D, decidí hacer esta plantilla para que todos tengan una base para comenzar. No hace falta que sea un proyecto de Blender, puede aplicarse con cualquier software. Muy influenciado por la charla de Andy Goralczyk ["The Production Pipeline of Spring"](https://www.youtube.com/watch?v=aR3yNNGK_sc). ¡Siéntase libre de usarlo y compartirlo como quiera! No hay necesidad de darme crédito :)

### Primer paso: Cambiarle el nombre a la carpeta principal con el nombre del proyecto. 

-Luego: 

--audio: va todo lo de audio: diálogos, foley, banda sonora ("ost"), y efectos de sonido ("sfx");

--docs: acá van todos los documentos para organizarse (planillas y desgloses en "management") y del guión ("script"), programas, addons/plugins y scripts ("software"), etc.;

--image: todas las imagenes, como alphas de pinceles ("alphas"), concept art ("concept"), el colorscript ("colorscript"), los HDRIs ("HDRI"), las luces IES ("IES"), mattes para los fondos ("mattes"), imágenes de referencia ("refs"), storyboard ("storyboard"), texturas no finales y cosas así ("textures");

--lib: van todos los modelos 3d y los rigs, junto a todo lo relacionado;

---char: acá van los personajes. Las geometrías/mallas usan un sufijo "mdl", los rigs un sufijo "rig", y los modelos con materiales un sufijo "shd" (por shading), mientras que la versión final del personaje a ser referenciada no usa sufijo, y las versiones viejas van en "old";

---envs: acá van los elementos que conforman los escenarios (arboles, montañas, muebles, señalización, edificios, etc.);

---props: acá van los assets/props (cosas con las que se va a interactuar);

---set: acá van los sets (archivos con envs, props, y chars ya rigueados linkeados/referenciados);

--scenes: acá van las distintas etapas de las tomas ("anim", "lit", "render"). Van todas las escenas del proyecto numeradas cronologicamente en su propia carpeta de la forma "01-nombre-de-escena" (número que indica el orden y nombre que describe la escena). En el caso de un proyecto episódico, recomiendo crear carpetas correspondientes a cada episodio y dentro de ellas las escenas correspondientes al episodio;

---01-scene-name: acá van todas las tomas de cada escena en particular, numeradas cronologicamente en su propia carpeta de la forma "0010" (número de la toma -en incrementos de 10, por si hace falta agregar tomas entremedio-);

----0010:  

-----anim: todas las versiones de la toma sólo de animación, nombradas con la forma "01_0010_ANIM_A" (número de la escena, número de la toma -de 10 en 10, por si hace falta agregar tomas entremedio-, nombre de la etapa, y versión de la toma -usando una letra-);

-----lit: todas las versiones de la toma ya animada, con su iluminación apropiada, nombradas con la forma "01_0010_LIT_A";

-----render: todos los frames renderizados (van en "frames", 01_0010_RENDER_0000.png), y los plates, si corresponde (van en "plates", 01_0010_RENDER_plate_A.png);

--video: acá van todos los videos;

---animatic: el animatic.blend del animatic (animatic_A.blend, etc.), y la carpeta para render del animatic ("render"); 

---compo: el compo.blend que combina todo (compo_A.blend, etc.); 

---layout: el layout.blend del layout (layout_A.blend, etc.), y la carpeta para render del layout ("render"); 

---master: video render final que sale del compo.blend;

---refs: videos de referencia de todo tipo;


PD: Sentirse libre de modificarla como les convenga en su proyecto. Es más que nada una referencia a tener en cuenta. Si tienen alguna sugerencia, sientanse libres de decírmelo en un Issue de acá o en las redes sociales (@andycuccaro).

___

```
./
├── audio/
│   ├── dialogue/
│   │   ├── dialogue-track-01-scene-name_0010.type
│   │   ├── dialogue-track-02-another-scene-name_0030.type
│   │   └── dialogue-track-03-yet-another-scene-name_0050.type
│   ├── foley/
│   │   ├── foley-track-01-scene-name_0010.type
│   │   ├── foley-track-02-another-scene-name_0030.type
│   │   └── foley-track-03-yet-another-scene-name_0050.type
│   ├── ost/
│   │   ├── soundtrack-01-scene-name_0010.type
│   │   ├── soundtrack-02-another-scene-name_0030.type
│   │   └── soundtrack-03-yet-another-scene-name_0050.type
│   └── sfx/
│       ├── sfx-track-01-scene-name_0010.type
│       ├── sfx-track-02-another-scene-name_0030.type
│       └── sfx-track-03-yet-another-scene-name_0050.type
├── docs/
│   ├── fonts/
│   │   ├── a-font-name.otf
│   │   └── another-font-name.ttf
│   ├── management/
│   │   ├── Difficulty-Spreadsheet.ods*
│   │   ├── Production-Breakdown.ods*
│   │   └── Production-Spreadsheet.ods*
│   ├── script/
│   │   ├── changes/
│   │   ├── Script-Name_A.odt
│   │   ├── Script-Name_B.odt
│   │   └── Script-Name.odt
│   └── software/
│       ├── binaries/
│       └── plugins/
├── img/
│   ├── alphas/
│   │   └── test
│   ├── colorscript/
│   │   └── test
│   ├── concepts/
│   │   └── test
│   ├── HDRI/
│   │   └── test
│   ├── IES/
│   │   └── test
│   ├── mattes/
│   │   └── test
│   ├── refs/
│   │   ├── char/
│   │   ├── envs/
│   │   ├── palette/
│   │   ├── props/
│   │   └── style/
│   ├── storyboard/
│   │   └── sketches/
│   │       └── test
│   └── textures/
│       └── test
├── lib/
│   ├── char/
│   │   ├── old/
│   │   │   ├── char-01-name_mdl_A.type
│   │   │   ├── char-01-name_mdl_B.type
│   │   │   ├── char-01-name_mdl_C.type
│   │   │   ├── char-01-name_rig_A.type
│   │   │   ├── char-01-name_rig_B.type
│   │   │   ├── char-01-name_rig_C.type
│   │   │   ├── char-01-name_shd_A.type
│   │   │   ├── char-01-name_shd_B.type
│   │   │   ├── char-01-name_shd_C.type
│   │   │   ├── char-02-name_mdl_A.type
│   │   │   ├── char-02-name_mdl_B.type
│   │   │   ├── char-02-name_mdl_C.type
│   │   │   ├── char-02-name_rig_A.type
│   │   │   ├── char-02-name_rig_B.type
│   │   │   ├── char-02-name_rig_C.type
│   │   │   ├── char-02-name_shd_A.type
│   │   │   ├── char-02-name_shd_B.type
│   │   │   ├── char-02-name_shd_C.type
│   │   │   ├── char-03-name_mdl_A.type
│   │   │   ├── char-03-name_mdl_B.type
│   │   │   ├── char-03-name_mdl_C.type
│   │   │   ├── char-03-name_rig_A.type
│   │   │   ├── char-03-name_rig_B.type
│   │   │   ├── char-03-name_rig_C.type
│   │   │   ├── char-03-name_shd_A.type
│   │   │   ├── char-03-name_shd_B.type
│   │   │   └── char-03-name_shd_C.type
│   │   ├── char-01-name_mdl.type
│   │   ├── char-01-name_rig.type
│   │   ├── char-01-name_shd.type
│   │   ├── char-01-name.type
│   │   ├── char-02-name_mdl.type
│   │   ├── char-02-name_rig.type
│   │   ├── char-02-name_shd.type
│   │   ├── char-02-name.type
│   │   ├── char-03-name_mdl.type
│   │   ├── char-03-name_rig.type
│   │   ├── char-03-name_shd.type
│   │   └── char-03-name.type
│   ├── envs/
│   │   ├── old/
│   │   │   ├── env-name_mdl_A.type
│   │   │   ├── env-name_mdl_B.type
│   │   │   ├── env-name_mdl_C.type
│   │   │   ├── env-name_shd_A.type
│   │   │   ├── env-name_shd_B.type
│   │   │   ├── env-name_shd_C.type
│   │   │   ├── some-another-env-name_mdl_A.type
│   │   │   ├── some-another-env-name_mdl_B.type
│   │   │   ├── some-another-env-name_mdl_C.type
│   │   │   ├── some-another-env-name_shd_A.type
│   │   │   ├── some-another-env-name_shd_B.type
│   │   │   ├── some-another-env-name_shd_C.type
│   │   │   ├── yet-another-env-name_mdl_A.type
│   │   │   ├── yet-another-env-name_mdl_B.type
│   │   │   ├── yet-another-env-name_mdl_C.type
│   │   │   ├── yet-another-env-name_shd_A.type
│   │   │   ├── yet-another-env-name_shd_B.type
│   │   │   └── yet-another-env-name_shd_C.type
│   │   ├── env-name_mdl.type
│   │   ├── env-name_shd.type
│   │   ├── env-name.type
│   │   ├── some-another-env-name_mdl.type
│   │   ├── some-another-env-name_shd.type
│   │   ├── some-another-env-name.type
│   │   ├── yet-another-env-name_mdl.type
│   │   ├── yet-another-env-name_shd.type
│   │   └── yet-another-env-name.type
│   ├── props/
│   │   ├── old/
│   │   │   ├── a-prop-name_mdl_A.type
│   │   │   ├── a-prop-name_mdl_B.type
│   │   │   ├── a-prop-name_mdl_C.type
│   │   │   ├── a-prop-name_rig_A.type
│   │   │   ├── a-prop-name_rig_B.type
│   │   │   ├── a-prop-name_rig_C.type
│   │   │   ├── a-prop-name_shd_A.type
│   │   │   ├── a-prop-name_shd_B.type
│   │   │   ├── a-prop-name_shd_C.type
│   │   │   ├── other-prop-name_mdl_A.type
│   │   │   ├── other-prop-name_mdl_B.type
│   │   │   ├── other-prop-name_mdl_C.type
│   │   │   ├── other-prop-name_rig_A.type
│   │   │   ├── other-prop-name_rig_B.type
│   │   │   ├── other-prop-name_rig_C.type
│   │   │   ├── other-prop-name_shd_A.type
│   │   │   ├── other-prop-name_shd_B.type
│   │   │   ├── other-prop-name_shd_C.type
│   │   │   ├── some-prop-name_mdl_A.type
│   │   │   ├── some-prop-name_mdl_B.type
│   │   │   ├── some-prop-name_mdl_C.type
│   │   │   ├── some-prop-name_rig_A.type
│   │   │   ├── some-prop-name_rig_B.type
│   │   │   ├── some-prop-name_rig_C.type
│   │   │   ├── some-prop-name_shd_A.type
│   │   │   ├── some-prop-name_shd_B.type
│   │   │   └── some-prop-name_shd_C.type
│   │   ├── a-prop-name_mdl.type
│   │   ├── a-prop-name_rig.type
│   │   ├── a-prop-name_shd.type
│   │   ├── a-prop-name.type
│   │   ├── other-prop-name_mdl.type
│   │   ├── other-prop-name_rig.type
│   │   ├── other-prop-name_shd.type
│   │   ├── other-prop-name.type
│   │   ├── some-prop-name_mdl.type
│   │   ├── some-prop-name_rig.type
│   │   ├── some-prop-name_shd.type
│   │   └── some-prop-name.type
│   ├── set/
│   │   ├── old/
│   │   │   ├── 01-set-name_A.type
│   │   │   ├── 01-set-name_B.type
│   │   │   ├── 01-set-name_C.type
│   │   │   ├── 02-set-name_A.type
│   │   │   ├── 02-set-name_B.type
│   │   │   ├── 02-set-name_C.type
│   │   │   ├── 03-set-name_A.type
│   │   │   ├── 03-set-name_B.type
│   │   │   └── 03-set-name_C.type
│   │   ├── 01-set-name.type
│   │   ├── 02-set-name.type
│   │   └── 03-set-name.type
│   └── shd/
│       ├── old/
│       │   ├── a-shader-name_A.type
│       │   ├── a-shader-name_B.type
│       │   ├── a-shader-name_C.type
│       │   ├── other-shader-name_A.type
│       │   ├── other-shader-name_B.type
│       │   ├── other-shader-name_C.type
│       │   ├── some-shader-name_A.type
│       │   ├── some-shader-name_B.type
│       │   └── some-shader-name_C.type
│       ├── a-shader-name.type
│       ├── other-shader-name.type
│       └── some-shader-name.type
├── scenes/
│   ├── anim/
│   │   ├── 01-scene-name/
│   │   │   ├── 0010/
│   │   │   │   ├── 01_0010_ANIM_A.type
│   │   │   │   ├── 01_0010_ANIM_B.type
│   │   │   │   └── 01_0010_ANIM.type
│   │   │   └── 0020/
│   │   │       ├── 01_0020_ANIM_A.type
│   │   │       ├── 01_0020_ANIM_B.type
│   │   │       └── 01_0020_ANIM.type
│   │   ├── 02-another-scene-name/
│   │   │   └── 0030/
│   │   │       ├── 02_0030_ANIM_A.type
│   │   │       ├── 02_0030_ANIM_B.type
│   │   │       └── 02_0030_ANIM.type
│   │   └── 03-yet-another-scene-name/
│   │       ├── 0040/
│   │       │   ├── 03_0040_ANIM_A.type
│   │       │   ├── 03_0040_ANIM_B.type
│   │       │   └── 03_0040_ANIM.type
│   │       ├── 0050/
│   │       │   ├── 03_0050_ANIM_A.type
│   │       │   ├── 03_0050_ANIM_B.type
│   │       │   └── 03_0050_ANIM.type
│   │       └── 0060/
│   │           ├── 03_0060_ANIM_A.type
│   │           ├── 03_0060_ANIM_B.type
│   │           └── 03_0060_ANIM.type
│   ├── lit/
│   │   ├── 01-scene-name/
│   │   │   ├── 0010/
│   │   │   │   ├── 01_0010_ANIM_A.type
│   │   │   │   ├── 01_0010_ANIM_B.type
│   │   │   │   └── 01_0010_ANIM.type
│   │   │   └── 0020/
│   │   │       ├── 01_0020_ANIM_A.type
│   │   │       ├── 01_0020_ANIM_B.type
│   │   │       └── 01_0020_ANIM.type
│   │   ├── 02-another-scene-name/
│   │   │   └── 0030/
│   │   │       ├── 02_0030_ANIM_A.type
│   │   │       ├── 02_0030_ANIM_B.type
│   │   │       └── 02_0030_ANIM.type
│   │   └── 03-yet-another-scene-name/
│   │       ├── 0040/
│   │       │   ├── 03_0040_ANIM_A.type
│   │       │   ├── 03_0040_ANIM_B.type
│   │       │   └── 03_0040_ANIM.type
│   │       ├── 0050/
│   │       │   ├── 03_0050_ANIM_A.type
│   │       │   ├── 03_0050_ANIM_B.type
│   │       │   └── 03_0050_ANIM.type
│   │       └── 0060/
│   │           ├── 03_0060_ANIM_A.type
│   │           ├── 03_0060_ANIM_B.type
│   │           └── 03_0060_ANIM.type
│   └── render/
│       ├── frames/
│       │   ├── 01-scene-name/
│       │   │   ├── 0010/
│       │   │   │   ├── 01_0010_RENDER_0001.type
│       │   │   │   ├── 01_0010_RENDER_0002.type
│       │   │   │   └── 01_0010_RENDER_0003.type
│       │   │   └── 0020/
│       │   │       ├── 01_0020_RENDER_0001.type
│       │   │       ├── 01_0020_RENDER_0002.type
│       │   │       └── 01_0020_RENDER_0003.type
│       │   ├── 02-another-scene-name/
│       │   │   └── 0030/
│       │   │       ├── 02_0030_RENDER_0001.type
│       │   │       ├── 02_0030_RENDER_0002.type
│       │   │       └── 02_0030_RENDER_0003.type
│       │   └── 03-yet-another-scene-name/
│       │       ├── 0040/
│       │       │   ├── 03_0040_RENDER_0001.type
│       │       │   ├── 03_0040_RENDER_0002.type
│       │       │   └── 03_0040_RENDER_0003.type
│       │       ├── 0050/
│       │       │   ├── 03_0050_RENDER_0001.type
│       │       │   ├── 03_0050_RENDER_0002.type
│       │       │   └── 03_0050_RENDER_0003.type
│       │       └── 0060/
│       │           ├── 03_0060_RENDER_0001.type
│       │           ├── 03_0060_RENDER_0002.type
│       │           └── 03_0060_RENDER_0003.type
│       └── plate/
│           ├── 01-scene-name/
│           │   ├── 0010/
│           │   │   ├── 01_0010_RENDER_plate_A.type
│           │   │   ├── 01_0010_RENDER_plate_B.type
│           │   │   └── 01_0010_RENDER_plate.type
│           │   └── 0020/
│           │       ├── 01_0020_RENDER_plate_A.type
│           │       ├── 01_0020_RENDER_plate_B.type
│           │       └── 01_0020_RENDER_plate.type
│           ├── 02-another-scene-name/
│           │   └── 0030/
│           │       ├── 02_0030_RENDER_plate_A.type
│           │       ├── 02_0030_RENDER_plate_B.type
│           │       └── 02_0030_RENDER_plate.type
│           └── 03-yet-another-scene-name/
│               ├── 0040/
│               │   ├── 03_0040_RENDER_plate_A.type
│               │   ├── 03_0040_RENDER_plate_B.type
│               │   └── 03_0040_RENDER_plate.type
│               ├── 0050/
│               │   ├── 03_0050_RENDER_plate_A.type
│               │   ├── 03_0050_RENDER_plate_B.type
│               │   └── 03_0050_RENDER_plate.type
│               └── 0060/
│                   ├── 03_0060_RENDER_plate_A.type
│                   ├── 03_0060_RENDER_plate_B.type
│                   └── 03_0060_RENDER_plate.type
├── video/
│   ├── animatic/
│   │   ├── render/
│   │   │   ├── frame_0001.type
│   │   │   ├── frame_0002.type
│   │   │   └── frame_0003.type
│   │   ├── animatic_A.type
│   │   ├── animatic_B.type
│   │   └── animatic.type
│   ├── compo/
│   │   ├── render/
│   │   │   ├── frame_0001.type
│   │   │   ├── frame_0002.type
│   │   │   └── frame_0003.type
│   │   ├── compo_A.type
│   │   ├── compo_B.type
│   │   └── compo.type
│   ├── layout/
│   │   ├── render/
│   │   │   ├── frame_0001.type
│   │   │   ├── frame_0002.type
│   │   │   └── frame_0003.type
│   │   ├── layout_A.type
│   │   ├── layout_B.type
│   │   └── layout.type
│   ├── master/
│   │   └── movie_master.type
│   └── refs/
│       ├── a-video-ref.type
│       ├── other-video-ref.type
│       └── some-video-ref.type
├── file.txt
└── README.md
```
